# isp_spinal model
# the isp_spinal model produces an isp with a long "spine", then a number of
# internal "redundancies", which are 
from mergexp import * 

net = Network('spinal', routing == static, addressing == ipv4)
# Spinal ISP
spine_len = 4 
spine = [net.node('spinal_node_{:02d}'.format(i)) for i in range(0, spine_len)]
for i in range(0, spine_len - 1):
    net.connect([spine[i], spine[i+1]])
link_node_a = net.node('link_node_1')
link_node_b = net.node('link_node_2')
net.connect([link_node_a, spine[0], spine[1], spine[2]])
net.connect([link_node_b, spine[1], spine[2], spine[3]])
iaps = [net.node('iap:{:02d}'.format(i)) for i in range(0, 4)]
net.connect([iaps[0], spine[0]])
net.connect([iaps[1], spine[2]])
net.connect([iaps[2], spine[2]])
net.connect([iaps[3], spine[3]])
experiment(net)
