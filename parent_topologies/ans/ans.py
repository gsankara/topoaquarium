# In situ topology selected from the topology zoo
# Manually constructed from the ANS Network (http://www.topology-zoo.org/files/Ans.gml) 
from mergexp import * 
net = Network('ans', routing == static, addressing == ipv4)
# Derived from the ANS topology in the topologyu zoo 
node_names = ['hartford', 'nyc', 'chicago', 'cleveland', 'greensboro', 'atlanta', 'dc', 'reston', 'dallas', 'stl', 'seattle', 'denver', 'sf', 'sj','la', 'abq', 
    'hawaii', 'houston']
nodes = {n: net.node(n) for n in node_names}
# Source 0: 1,3
for i in ['nyc','cleveland']:
    net.connect([nodes['hartford'], nodes[i]])
# Source 1: 3,6,7
for i in ['cleveland','dc','reston']:
    net.connect([nodes['nyc'], nodes[i]])
# Source 2: 3,9,11
for i in ['stl','denver']:
    net.connect([nodes['chicago'], nodes[i]])
# Source 4: 5,6
for i in ['atlanta', 'dc']:
    net.connect([nodes['greensboro'], nodes[i]])
# Source 5: 17
net.connect([nodes['atlanta'], nodes['houston']])
# Source 6: 7 
net.connect([nodes['dc'], nodes['reston']])
# Source 7: 8 
net.connect([nodes['reston'], nodes['dallas']])
# Source 8:9,13,17
for i in ['stl','sj','houston']:
    net.connect([nodes['dallas'], nodes[i]])
# Source 10: 11,12
for i in ['denver','sf']:
    net.connect([nodes['seattle'], nodes[i]])
# Source 11: 12
net.connect([nodes['denver'], nodes['sf']])
# Source 12: 13, 14
for i in ['la','abq']:
    net.connect([nodes['sf'], nodes[i]])
# Source 14: 15
net.connect([nodes['abq'], nodes['hawaii']])
# Source 15: 16,17
for i in ['hawaii','houston']:
    net.connect([nodes['abq'], nodes[i]])
experiment(net)

