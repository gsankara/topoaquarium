# max flow

# $python maxflow.py 3 1,2,3 2 2,4 1,0,1/1,1,0
import networkx as nx
from copy import deepcopy

if __name__ == "__main__":
  from os import sys
  nF = int(sys.argv[1])
  nM = int(sys.argv[3])
  # break by ;
  flowrates = [float(i) for i in sys.argv[2].split(",")]
  capacity = [float(i) for i in sys.argv[4].split(",")]
  interconnect =[[int(i) for i in warr.split(",")] for warr in sys.argv[5].split("/")]

import matplotlib.pyplot as plt
import datetime
def debugG(Gp, eLbl, eM):
  pos = {}
  gn = [n for n in Gp.nodes if n.startswith("f") or n.startswith("m")]
  G = nx.DiGraph(nx.subgraph(Gp,gn))
  # prune edges
  for i in range(0,len(eM)): # flow index
    for j in range(0,len(eM[0])): # monitor index
      mn = "m{val:n}".format(val=j)
      fn = "f{val:n}".format(val=i)
      if eM[i][j] == 0 and fn in G[mn]:
        G.remove_edge(mn,fn)
      
  colors=[G.nodes[nd]['color'] for nd in G.nodes]
  for nd in G.nodes:
    pos[nd]=G.nodes[nd]['pos']
  #print (pos)
  #pos = nx.spring_layout(G)
  nx.draw_networkx(G, pos, node_size=200, node_color=colors, font_size=8, font_weight='bold', alpha=0.4 )
  nx.draw_networkx_edge_labels(G, pos, edge_labels=eLbl, label_pos=0.9, font_size=5)
  nx.draw_networkx_edge_labels(G, pos, edge_labels=eLbl, label_pos=0.5, font_size=5)
  nx.draw_networkx_edge_labels(G, pos, edge_labels=eLbl, label_pos=0.1, font_size=5)
  plt.tight_layout()
  ct = str(datetime.datetime.now()).replace(" ", "-")
  plt.savefig("maxflow{}.png".format(ct),format="PNG")
  #print (G.nodes, G.edges)
  for ed in G.edges:
    if 'weight' in G.get_edge_data(*ed):
      print (ed, G.get_edge_data(*ed))

import math
from numpy import array as na
import time

def maxflow(nF, nM, flowrates, capacity, interconnect,debug=False):
  mid = time.time()
  G=nx.DiGraph()
  G.add_node("s", pos=(1,math.floor(nM/2)), color=(1,1,0))
  G.add_node("t", pos=(4,math.floor(nF/2)), color=(1,1,0))
  fNodes = []
  print ("####Given####")

  for i in range(nF):
    fn = "f{val:n}".format(val=i)
    if debug == True:
      print ("creating flow node ", fn, "with rate", flowrates[i])
    G.add_node(fn, pos=(2,i*2), color=(0,1,0))
    G.add_edge(fn, "t", weight=flowrates[i])
    
  for i in range(nM):
    mn = "m{val:n}".format(val=i)
    if debug == True:
      print ("creating monitor node ", mn, "with capacity", capacity[i])
    G.add_node(mn, pos=(1,i*2*nF/nM), color=(0,0,1))
    G.add_edge("s", mn,weight=capacity[i]) 

  #print ("interconnection: " , interconnect)
  #print ("total flow rate", sum(flowrates))
  #print ("Monitor capacity total", sum(capacity))

  for j in range(nM):
    mn = "m{val:n}".format(val=j)
    for i in range(nF):
      fn = "f{val:n}".format(val=i)
      #print ("current i, j ", i, j)
      if interconnect[j][i] > 0:
           G.add_edge (mn, fn) # unweighted

  fV,fD = nx.maximum_flow(G, "s", "t", capacity="weight")
  
  sD={}
  residual = deepcopy(capacity)
  print ("####Solution####")
  print("#Max flow:",fV)
  #print ("#Flow mappings")
  rD =[[0 for i in range(nM)] for j in range(nF)]
  mD =[[0 for i in range(nF)] for j in range(nM)]
  for j in range(nM):
    mn = "m{val:n}".format(val=j)
    for i in range(nF):
      fn = "f{val:n}".format(val=i)
      if fn in fD[mn] and fD[mn][fn]>0:
        residual[j]-=fD[mn][fn]
        #if debug == True:
        print ("#DEBUG1 id {} flow {} rate {:.3f}/{:.3f} mapped to monitor {} residual {:.3f}/{:.3f}".format(mid,fn,fD[mn][fn],flowrates[i],mn,residual[j],capacity[j]))
        sD[(mn,fn)]="{:.3f}".format(fD[mn][fn])
        rD[i][j]=sD[(mn,fn)]
        mD[j][i]=int(fD[mn][fn]*10000)/10000

  if debug == True:
    print ("#Flow mappings", na(rD))
  #print("#DEBUG0 Monitor utilization", mD)
  vv = [int(sum(i)*10000)/10000 for i in mD]
  print("#DEBUG0 Monitor utilization vv", vv)
  #debugG(G, sD, rD)
  return fV, rD

if __name__ == "__main__":
  maxflow (nF, nM, flowrates, capacity, interconnect)

#print(fD)

############ sample output ################
##$ python maxflow.py 3 1,2,3 2 2,4 1,0,1/1,1,0
##creating flow node  f0
##creating flow node  f1
##creating flow node  f2
##creating monitor node  m0
##creating monitor node  m1
##interconnection:  [[1, 0, 1], [1, 1, 0]]
##['s', 't', 'f0', 'f1', 'f2', 'm0', 'm1'] [('s', 'm0'), ('s', 'm1'), ('f0', 't'), ('f1', 't'), ('f2', 't'), ('m0', 'f0'), ('m0', 'f2'), ('m1', 'f0'), ('m1', 'f1')]
##('s', 'm0') {'weight': 2}
##('s', 'm1') {'weight': 4}
##('f0', 't') {'weight': 1}
##('f1', 't') {'weight': 2}
##('f2', 't') {'weight': 3}
##('m0', 'f0') {}
##('m0', 'f2') {}
##('m1', 'f0') {}
##('m1', 'f1') {}
##5
##{'s': {'m0': 2, 'm1': 3}, 't': {}, 'f0': {'t': 1}, 'f1': {'t': 2}, 'f2': {'t': 2}, 'm0': {'f0': 0, 'f2': 2}, 'm1': {'f0': 1, 'f1': 2}}
##gsankara@DESKTOP-Q9R0SOR:/mnt/c/Users/Ganesh C S/Documents/usc/mike$ python maxflow.py 3 1.1,2.2,3.2 2 2.4,4.3 1,0,1/1,1,1
##creating flow node  f0
##creating flow node  f1
##creating flow node  f2
##creating monitor node  m0
##creating monitor node  m1
##interconnection:  [[1, 0, 1], [1, 1, 1]]
##['s', 't', 'f0', 'f1', 'f2', 'm0', 'm1'] [('s', 'm0'), ('s', 'm1'), ('f0', 't'), ('f1', 't'), ('f2', 't'), ('m0', 'f0'), ('m0', 'f2'), ('m1', 'f0'), ('m1', 'f1'), ('m1', 'f2')]
##('s', 'm0') {'weight': 2.4}
##('s', 'm1') {'weight': 4.3}
##('f0', 't') {'weight': 1.1}
##('f1', 't') {'weight': 2.2}
##('f2', 't') {'weight': 3.2}
##('m0', 'f0') {}
##('m0', 'f2') {}
##('m1', 'f0') {}
##('m1', 'f1') {}
##('m1', 'f2') {}
##6.5
##{'s': {'m0': 2.2000000000000006, 'm1': 4.3}, 't': {}, 'f0': {'t': 1.1}, 'f1': {'t': 2.2}, 'f2': {'t': 3.2}, 'm0': {'f0': 0, 'f2': 2.2000000000000006}, 'm1': {'f0': 1.1, 'f1': 2.2, 'f2': 0.9999999999999996}}
##gsankara@DESKTOP-Q9R0SOR:/mnt/c/Users/Ganesh C S/Documents/usc/mike$
