import networkx as nx
# utility methods
from mymergexp import get_graph as get_mx_graph
def get_graph (method, params):
  if method == 'get_mx_graph':
    method = globals()[method]
  else:
    method = getattr(nx,method)
  return method(*params)

def debugGraph (Gnp):
  print ("CONNECTED", nx.is_connected(Gnp), len(Gnp.nodes), len(Gnp.edges))

if __name__ == "__main__":
  file="archive/Oxford.graphml"
  G = get_graph ("read_graphml", [file])
  debugGraph(G)

  n,p,seed = 10,0.5,20
  G = get_graph ("gnp_random_graph", [n,p,seed])
  debugGraph(G)

  hsh = {"spine_len":35, "endpoint_len":10}
  G = get_graph ("get_mx_graph", ["eval/dogbone.py", hsh])
  debugGraph(G)

def get_monitors (method, params):
  return globals()[method](*params)

from random import sample, seed
def gmon_random (G, p, nseed=10):
  seed(nseed)
  n = int(p*G.number_of_nodes())
  return sample(G.nodes,n)

def gmon_degree (G, p):
  n = int(p*G.number_of_nodes())
  dList = G.degree(G)
  dMap = dict(zip(G.nodes, dList))
  dSorted = dict(sorted(dMap.items(), key=lambda item: item[1], reverse=True))
  return list(dSorted.keys())[0:n]

def gmon_degree_match (G, d, oper):
  m = []
  
  if oper == "ge":
   m = [i for i in G.nodes if G.degree[i] >=d]
  elif oper == "eq":
    m = [i for i in G.nodes if G.degree[i] ==d]
  elif oper == "le":
    m = [i for i in G.nodes if G.degree[i] <=d]
  #else:
    # not defined
  dMap = dict(G.degree(m)) # dict(zip(m, G.degree(m)))
  dSorted = dict(sorted(dMap.items(), key=lambda item: item[1], reverse=True))
  #print ("DBG dsorted " ,m, dSorted, dMap)
  return list(dSorted.keys())
  
def gmon_degree_skip (G, p, skip):
  n = int(p*G.number_of_nodes())
  nSkip = int(skip*G.number_of_nodes())
  dList = G.degree(G)
  dMap = dict(zip(G.nodes, dList))
  dSorted = dict(sorted(dMap.items(), key=lambda item: item[1], reverse=True))
  return list(dSorted.keys())[nSkip:n+nSkip]

## TODO
#def gmon_degree_mode (G, p, nthMode):
## method for 2nd and 3rd mode
##>>> l=[1,2,3,1,2,4,1,5,6]
##>>> from collections import Counter
##>>> lc=Counter(l)
##>>> lc
##Counter({1: 3, 2: 2, 3: 1, 4: 1, 5: 1, 6: 1})
##>>> Counter(l).most_common(3)[2]
##(3,1)
## list.index(value) and count
  
def debugMonitors(M):
  print ("DBGMonitors", M)

if __name__ == "__main__":
  M = get_monitors("gmon_random", [G,0.5,10])
  debugMonitors(M)

  M = get_monitors("gmon_degree", [G,0.5])
  debugMonitors(M)

  M = get_monitors("gmon_degree_skip", [G,0.5,0.25])
  debugMonitors(M)

from itertools import permutations as pm
from random import choices, uniform
def get_flows (method, params, debug=False):
  ## pick flow end points and the number of flows between the end points
  def flow_random (G, nF, seed=10):
    endpairs = list(pm (G.nodes,2))
    rates = [int(uniform(0.1,0.99)*1000)/1000 for _ in range(nF)]
    return choices(endpairs, k=nF), rates
  ## a file input with flows
  ## use mimic format CONN,fid,srcIP,srcport,->,dstIP,dstport,stime
  ##    Additionally compute rate
  ## connections from pcap file
  ##    Additionally compute rate
  ## sniff(offline='stackoverflow.pcap', session=TCPSession, store=False)
  ## https://stackoverflow.com/questions/35310104/can-reading-sessions-from-a-pcap-file-with-scapy-be-made-more-memory-efficient
  ## TODO save and debug
  def fdebug (flows, rates):
    count=0
    for fl in flows:
      print (fl, rates[count])
      count+=1
  print ("In flows")
  flows, rates = locals()[method](*params)
  if debug:
    fdebug (flows, rates)
  return flows, rates

if __name__ == "__main__":
  flows, rates = get_flows("flow_random", [G,20,10], True)

## TODO save and debug within all steps
from itertools import islice as isl
def get_paths (method, params, debug=False):
  ## compute K shortest paths
  ## compute distinct paths
  ## compute shortest paths given flow matrix
  
  def k_shortest (flows, G, k, weight_attr=None):
    paths = {}
    print ("in k shortest", G)
    for fl in flows:
      if not fl in paths:
        sp = nx.shortest_simple_paths(G,fl[0],fl[1],weight=weight_attr)
        paths[fl]=list(isl(sp, k))
    return paths

  def k_disjoint (flows, G, k, disj):
    paths = {}
    for fl in flows:
      if not fl in paths:
        ls =[]
        if "node" == disj:
          paths[fl] =list(isl(nx.node_disjoint_paths(G,fl[0],fl[1]),k))
        else:
          paths[fl] =list(isl(nx.edge_disjoint_paths(G,fl[0],fl[1]),k))
    return paths

  def debug(paths):
    for ep in paths.keys():
      print (ep, paths[ep])
  iflow, imethod = method.split("/")
  if "flow" != iflow:
    G = params[0]
    flows = list(pm (G.nodes,2))
  else:
    flows = params.pop(0)
  paths = locals()[imethod](flows, *params)
  print (iflow, params)
  if debug:
    debug(paths)
  
  return paths

if __name__ == "__main__":
  print ("Graph", G, G.is_multigraph())
  paths = get_paths ("all/k_shortest", [G, 2])
  paths = get_paths ("flow/k_disjoint", [flows, G, 2, "node"])
  paths = get_paths ("all/k_disjoint", [G, 2, "node"])
  paths = get_paths ("flow/k_disjoint", [flows, G, 2, "edge"])
  paths = get_paths ("all/k_disjoint", [G, 2, "edge"])
  paths = get_paths ("flow/k_shortest", [flows, G, 2])

import random as rnd
def get_monitor_capacity(method, params):
  precision = 1000
  def random_capacity (totalrate, scale, nM, rseed=10):
    rnd.seed(rseed)
    mrates = [int(uniform(0.1,0.99)*precision*scale*totalrate/nM)/precision for _ in range(nM)]
    return mrates
  def debug (mrates):
    print ("monitor capacity", mrates)
    
  mrates = []
  if method == "random_capacity":
    rates = params.pop(0)
    totalrate = sum(rates)
    mrates = locals()[method](totalrate, *params)
  else:
    mrates = locals()[method](*params)
  ## TODO define a file input for monitors and rates - to decide together ??
  debug (mrates)
  return mrates
if __name__ == "__main__":
  scale=1
  mrates = get_monitor_capacity ("random_capacity", [rates,4,len(M)])

## flow to path mapping
from random import choices, uniform
from bisect import bisect_left as bsearch
def get_flow_path_mapping(method, params, debug=False):
  
  def random_assign (flows,paths,kpaths):
    assign={}
    nF = len(flows)
    rndvalues =choices(range(0,kpaths),k=nF)
    debg = [len(paths[fl]) for fl in flows]
    print ("Assign",len(rndvalues), nF, len(debg), debg)
    
    assign = [paths[fl][rv%len(paths[fl])] for rv,fl in zip(rndvalues,flows)]
    return assign
  def round_robin (flows, paths):
    fcountmap = {} # counter tracks flow hit
    assign = {}
    for fl in flows:
      if not fl in fcountmap:
        fcountmap[fl]=0
      assign[fl] = paths[fl][fcountmap[fl]%len(paths[fl])]
      fcountmap[fl]+=1
    return assign
  # TODO round robin on paths
  # REMOVE paths is a list: need a heuristic for path selection
  ##  def round_robin_path (flows, paths):
  ##    pcountmap = {} # counter tracks flow hit
  ##    assign = {}
  ##    for fl in flows:
  ##      common_path = paths[fl][1:-1] # expecting atleast one hop path
  ##      if not common_path in pcountmap:
  ##        pcountmap[common_path]=0
  ##      assign[fl] = paths[pcountmap[common_path]%len(paths[fl])]
  ##      countmap[common_path]+=1
  ##    return assign

  def weighted (flows,paths, k , factor=2):
    # first path factor x^n-1, second x^n-2, last x^0=1 etc
    total=0
    buckets=[total:=total+factor**i for i in range(0,k)]
    fcountmap = {} # counter tracks flow hit
    assign = {}
    for fl in flows:
      if not fl in fcountmap:
        fcountmap[fl]=buckets[len(paths[fl])-1] # max value
      idx = bsearch(buckets, fcountmap[fl]%buckets[len(paths[fl])-1])
      assign[fl] = paths[fl][idx]
      if fcountmap[fl] > 0:
        fcountmap[fl]-=1
      else:
        fcountmap[fl]=buckets[len(paths[fl])-1] # max value
    return assign

  def debug(assign):
    for fl in assign.keys():
      #print ("Flow", fl, " maps to path " , assign[fl])
      print (".", end='')
    return
  assign = locals()[method](*params)
  if debug == True:
    debug (assign)
  return assign

if __name__ == "__main__":
  kpaths=2
  assign = get_flow_path_mapping ("random_assign", [flows,paths,kpaths])
  assign = get_flow_path_mapping ("round_robin", [flows,paths])
  assign = get_flow_path_mapping ("weighted", [flows,paths,kpaths])
## flow to path mapping - round robin, random, weight based

## path coverage based monitor selection
## take every path and randomly select a few nodes as monitors

## - maxflow??

def create_flow_monitor_matrix (M, assign):
  arr_of_arr = []
  for idx,pth in enumerate(assign):
    lst = [1 if m in assign[idx] else 0 for m in M ]
    #print ("DBG cfmm ", pth, assign[pth], lst)
    #exit(0)
    arr_of_arr.append(lst)
  return arr_of_arr

if __name__ == "__main__":
  Fsm = create_flow_monitor_matrix(M,assign)
  print (Fsm)
    
# path coverage check returns the column at which path coverage is satisfied
import numpy as np

def path_coverage_check(Fm):
  Fsm = np.matrix(Fm)
  nrow,ncol=Fsm.shape
  cover = np.array(nrow)
  #print ("init", np.count_nonzero(Fsm))
  for col in range(0,ncol):
    cover = np.logical_or(Fsm[:,col],cover)
  #print ("path coverage", Fsm, cover)
  return np.count_nonzero(cover)/nrow

if __name__ == "__main__":
  nthcol = path_coverage_check(Fsm)
  print (nthcol)
# TODO adjust the number of monitors to get full path coverage before maxflow

# duplicate percentage (data)
# failure
##def



## load, save, debug
import pickle

if __name__ == "__main__":
  pickle_variables = {"paths":paths, "mrates":mrates, "flows":flows,
                    "rates" :rates, "M": M , "G":G, "flow-path-assign":assign,
                    "flow-matrix": Fsm}

def pickle_save (filename,pickle_variables):
  #global pickle_variables
  with open(filename, "wb") as file:
    pickle.dump(pickle_variables, file)

def pickle_load (filename,pickle_variables):
  #global pickle_variables
  with open(filename, "rb") as file:
    pickle_variables=pickle.load(file)

def pickle_debug (filename,pickle_variables):
  #global pickle_variables
  with open(filename, "rb") as file:
    lvariables=pickle.load(file)
  for var in lvariables.keys():
    if var in pickle_variables:
      print (var, "found ", lvariables[var])
    else:
      print (var, "not found ", lvariables[var])

if __name__ == "__main__":  
  pickle_save("flow1541030324.pickle",pickle_variables)
  pickle_debug("flow1541030324.pickle",pickle_variables)
  pickle_load("flow1541030324.pickle",pickle_variables)
