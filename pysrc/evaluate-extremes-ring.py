##take a parameterized network 
##generate graph of different sizes
##  generate flows for each graph
##  get monitor sequence
##  (binary) search on monitor sequence length (0-1)
##    compute path coverage;
##      note coverage (modify to %)
##    compute maxflow;
##      note total rate vs flow rate
##      compute duplicate flow mapping between flows and monitors

from maxflow import *
from monitor_utils import *
from numpy import count_nonzero as cnzero0
from itertools import permutations as pm
import time

def cnzero(arr):
  arr1 = [1 for a in arr if float(a) > 0]
  return len(arr1)

for slen in [4,40]: #range(35,85,10):
  hsh = {"ring_size" : slen, "subnet_size" : 2, "subnet_pos" : [0,2]} #ring_size = 4, subnet_size = 2, subnet_pos = [0,2]
  G = get_graph ("get_mx_graph", ["topo/ans.py", hsh])
  debugGraph(G)

  paths = get_paths ("all/k_shortest", [G, 2]) # change last arg to 2 or higher based on graph

  Mt = get_monitors("gmon_random", [G,1,10])
  debugMonitors(Mt)

  for nF in [200,500]: #range(200,500,50):
    flows, rates = get_flows("flow_random", [G,nF,10], False)
    lfactor = 3
    lmin, lmax = int(len(Mt)/lfactor), len(Mt)
    lstep = int((lmax - lmin)/10)
    if lstep < 1:
      lstep = 1

    for l in [lmin,lmax]: #range(lmin,lmax,lstep):
      tstart = time.time()
      scale=4 # modify to higher value
      M = Mt[0:l]
      #print ("#DEBUG0 parameters graph, spine len, #flows, #monitors", G, slen, nF, l)
      
      mrates = get_monitor_capacity ("random_capacity", [rates,scale,len(M)])

      kpaths=2
      assign = get_flow_path_mapping ("random_assign", [flows,paths,kpaths],False)

      #print ("DEBUG Monitors", assign)
      print ("#DEBUG0 Monitors", M)
      print ("#DEBUG0 Monitor capacity", mrates)
      Fsm = create_flow_monitor_matrix(M,assign)
      print ("Flow monitor matrix", len(flows), len(assign), len(M), len(Fsm))
      #exit(0)

      nthcol = path_coverage_check(Fsm)
      print ("Path coverage", nthcol)

      FsmT = [[Fsm[j][i] for j in range(len(Fsm))] for i in range(len(Fsm[0]))]

      tR, Fmap = maxflow (len(assign), len(M), rates, mrates, FsmT,False)
      Fdup = [cnzero(fd) for fd in Fmap]
      dup = sum(Fdup)/len(Fmap)
      print ("Duplicates" , Fdup, dup, len(Fmap))
      tend = time.time()
      print ("#DEBUG0 parameters graph, graph params, #flows, #monitors, start time, end time", G, hsh, nF, l, tstart, tend, end=' ')
      print ("#DEBUG0 solution flow rate, monitor rate, path coverage, total rate, dup", "{:.2f}".format(sum(rates)), "{:.2f}".format(sum(mrates)), nthcol, "{:.5f}".format(tR), "{:.5f}".format(dup))
      #exit(0)
              
        
