#mymergexp
import networkx as nx

static = 0
routing = static
ipv4 = 1
addressing = ipv4
class Network(nx.graph.Graph):
  def __init__(self, name, routing=static, addressing=ipv4):
    self.net = nx.Graph(name=name)
  
  #node = super(Network,self).add_node
  #node(self,args) = self.net.add_node(args)
  def node (self,args):
    self.net.add_node (args)
    #print (self.net, self.net.nodes, nd, args)
    return args

##  connect = self.add_edge
  def connect(self,args):
    #print (args[0],args[1]) #,*args[1])
    # NOTE: TODO we can have more than 2 args and need to create links between all pairs
    return self.net.add_edge (args[0],args[1])

  def graph(self):
    return self.net


def experiment(x):
  #print (x.net) #x.nodes(data=True)) #, x.edges(data=True))
  return

import re
def get_graph(pyscript, rvars={}):
  #print ("R keys", rvars)
  with open(pyscript, "r") as pyf:
    scr = pyf.read().split("\n")
    
    rkeys = tuple(rvars.keys())
    #print ("R keys", rvars, rkeys)
    
    mscr = []
    for ln in scr:
      if ln.startswith("from mergexp"):
        continue
      # check if assignment statement
      elif ln.startswith(rkeys) and "=" in ln:
        tokens = ln.split()
        #print (tokens)
        tokens[2]=str(rvars[tokens[0]])
        ln = "".join(tokens)
        #print ("modified ", ln)
        mscr.append(ln)
      else:
        mscr.append(ln)
    scrm = "\n".join(mscr)
    #print ("--------------")
    #print(scrm)
    #print ("--------------")
    exec (scrm, globals())
    return net.graph()
    # remove import - we are already here

from os import sys
if __name__ == "__main__":
  hsh = {"spine_len":5, "endpoint_len":4}
  gr = get_graph(sys.argv[1],hsh)
  print (gr)
    
  
